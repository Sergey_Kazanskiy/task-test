<?php
class Route {

    public function run( $route ) {
        if( $_SERVER['HTTP_HOST'] != SITE ){
            header( "Location: http://".SITE );
        }
        
        $url = trim( $route, "/" );
        $segments_url = explode( "/", $url );
        $conf = null;

        $request_method = $_SERVER['REQUEST_METHOD'];
        if($request_method == 'POST') {
            $segments_url = preg_replace( "/\?.*/", "", $segments_url );
            $segments_url = array_diff( $segments_url, array('') );
        } else if ($request_method == 'GET') {
            $segments_url = preg_replace( "/\?.*/", "", $segments_url );
            $segments_url = array_diff( $segments_url, array('') );
        } else if ($request_method == 'PUT') {
            // Method is PUT
        } else if ($request_method == 'DELETE') {
            // Method is DELETE
        } else {
            // Method unknown
        }
        if( !isset( $segments_url[0] ) ) {
            $segments_url[0] = '';
        }
        
        //print_r( $segments_url );
        
        switch ( $segments_url[0] ) {
            case '':
                $conf = 'index';
                break;
            case 'login':
                $conf = 'login/login';
                break;
            case 'task':
                $conf = 'task/user';
                break;
        }
        
        $segments_conf = explode("/", $conf );
        
        if( !empty( $segments_conf[0] ) ) {
            $actions = array_merge(
                array_reverse( $segments_conf ),
                $segments_url
            );

            $controller_name_shift = array_shift( $actions );
            $controller_name = mb_convert_case( $controller_name_shift, MB_CASE_TITLE )."_Controller";

            include ( CONTROLLERS.$controller_name.".php" );
            $controller = new $controller_name();
            
            array_shift( $actions );
            if( !empty( $actions[0] ) ) {
                if( count( $actions ) > 1 ) {
                    $method = implode( '_', $actions );
                } else {
                    $method = $actions[0];
                }
                if( method_exists( $controller, $method ) ) {
                    $act = end( $actions );
                    $controller->$method( $act, $actions );
                } else {
                    header("HTTP/1.0 404 Not Found");
                    echo 'Err method: <strong>function '.$method.'(){}</strong> in <strong>'.$controller_name.'.php</strong>';                    
                    $layout = new Views();
                    $data = compact( 'route', 'actions' );
                    return $layout->render( VIEWS."pages/page404.php", $data );
                }
            } else {
                $controller->init();
            }
        } else {
            header("HTTP/1.0 404 Not Found");
            $layout = new Views();
            $data = compact( 'route', 'actions' );
            return $layout->render( VIEWS."pages/page404.php", $data );
        }
    }
}