<?php

class Controllers{
    public $admin;
    
    public function __construct(){
        $this->admin = 0;
        
        session_start();
        if( isset($_SESSION['admin']) ){
            $this->admin = 1;    
        }
    }
    //==============================================================================
	public function is_ajax_request() {
		if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
            $this->ajax = array(
                'POST'=>$_POST,
                'GET'=>$_GET,
                'PUT'=>$_PUT,
                'DELETE'=>$_DELETE
            );
            return true;  
		} else {
            return false;
		}
	}
    //==============================================================================
    public function getModel( $model_name=NULL ) {
        $this->model = $model_name;
        
        include_once ( MODELS.mb_convert_case( $this->model, MB_CASE_TITLE )."_Model.php" );
        $_model = mb_convert_case( $this->model, MB_CASE_TITLE )."_Model";
        $_model = new $_model;
        return $_model;
    }
    //==============================================================================
    public function tmpl( $name, $data, $layout=NULL ){
        $layout = new Views( $layout );
        return $layout->render( VIEWS.$name.".php", $data );
    }
    //==============================================================================
    public function tmpl_ajax( $path_name, $data ){
        $ajax_tmpl = new Views();
        return $ajax_tmpl->tmpl_ajax( VIEWS.$path_name.".php", $data );
    }
    //==============================================================================
    public function __404(){
        header("HTTP/1.0 404 Not Found");
        $layout = new Views();
        return $layout->render( VIEWS."pages/page404.php", $data );
    }

}