<style>th a{color:#fff;}th a:hover{color:#fff;}</style>
<script>
$(document).ready(function(){
    var url = window.location.href;
    var parser = document.createElement('a');
    parser.href = url;
    var search = parser.search.replace("?","");
        
    $(".page-item").click(function(){
        var page_value = false;
        var current = $(this).find("a").attr("href").split("=")[1];
        if( search != '' && search.match(/page/) ){
            $.each(search.split("&"), function(k,v){
                if( v.match(/page/) ){
                    page_value = v.split("=")[1];
                }
            });                
            search = search.replace("page="+page_value,"page="+current);
            $(this).find("a").attr("href","?"+search);
        } else {
            var current = $(this).find("a").attr("href").split("=")[1];
            search = "page="+current+search;
            $(this).find("a").attr("href","?"+search);
        }
    });
    
    $(".filter").click(function(){
        var fname = $(this).attr("id");
        $.each(search.split("&"), function(k,v){
            if( v.match(fname) ){
                sort_value = v.split("=")[1];
            }
        });
        if( search != "" && search.match(fname) ){
            if( sort_value == 'asc' ){
                search = search.replace(/&.*/,"") + "&" + fname + "=desc";
            } else {
                search = search.replace(/&.*/,"") + "&" + fname + "=asc";
            }
            $(this).attr("href","?"+search);            
        } else {
            search = search.replace(/&.*/,"") + "&" + fname + "=asc";
            $(this).attr("href","?"+search);
        }
    });

    $('.toast').toast();
});
</script>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <? if( $_GET['login'] == 'asc' ): ?>
      <th scope="col"><a class="filter" href="" id="login">Имя пользователя</a><i class="fa fa-fw fa-sort-up"></i></th>
      <? elseif( $_GET['login'] == 'desc' ): ?>
      <th scope="col"><a class="filter" href="" id="login">Имя пользователя</a><i class="fa fa-fw fa-sort-down"></i></th>
      <? else: ?>
      <th scope="col"><a class="filter" href="" id="login">Имя пользователя</a><i class="fa fa-fw fa-sort"></i></th>
      <? endif; ?>
      
      <? if( $_GET['email'] == 'asc' ): ?>
      <th scope="col"><a class="filter" id="email" href="">Email</a><i class="fa fa-fw fa-sort-up"></th>
      <? elseif( $_GET['email'] == 'desc' ): ?>
      <th scope="col"><a class="filter" id="email" href="">Email</a><i class="fa fa-fw fa-sort-down"></th>
      <? else: ?>
      <th scope="col"><a class="filter" id="email" href="">Email</a><i class="fa fa-fw fa-sort"></th>
      <? endif; ?>
      
      <th scope="col">Текст задачи</th>
      
      <? if( $_GET['status'] == 'asc' ): ?>
      <th scope="col"><a class="filter" id="status" href="">Статус</a><i class="fa fa-fw fa-sort-up"></th>
      <? elseif( $_GET['status'] == 'desc' ): ?>
      <th scope="col"><a class="filter" id="status" href="">Статус</a><i class="fa fa-fw fa-sort-down"></th>
      <? else: ?>
      <th scope="col"><a class="filter" id="status" href="">Статус</a><i class="fa fa-fw fa-sort"></th>
      <? endif; ?>
      
      <?if($data['is_admin']):?>
      <th scope="col"><i class="fa fa-pencil-square-o"></i></th>
      <?endif;?>
    </tr>
  </thead>
  <tbody>
    <? foreach( $data['tasks']['rows'] as $k=>$v ): ?>
    <tr>
      <th scope="row"><?=$v['id']?></th>
      <td><?=$v['login']?></td>
      <td><?=$v['email']?></td>
      <td><a href="/task/?id=<?=$v['id']?>"><?=mb_strimwidth( $v['task'], 0, 40, "..." )?></a></td>
      <?if( $v['modern'] == 1 && $v['complete'] == 0 ):?>
      <td><span class="badge badge-warning">Не выполнено</span>&nbsp;<span class="badge badge-success">Изменино</span></td>
      <?elseif( $v['modern'] == 1 && $v['complete'] == 1 ):?>
      <td><span class="badge badge-success">Выполнено</span>&nbsp;<span class="badge badge-success">Изменино</span></td>
      <?elseif( $v['modern'] == 0 && $v['complete'] == 1 ):?>
      <td><span class="badge badge-success">Выполнено</span></td>
      <?elseif( $v['modern'] == 1 && $v['complete'] == 0 ):?>
      <td><span class="badge badge-success">Изменино</span></td>
      <?else:?>
      <td><span class="badge badge-warning">Не выполнено</span></td>
      <?endif;?>
      <?if($data['is_admin']):?>
      <td><a href="/task/edit/?id=<?=$v['id']?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
      <?endif;?>
    </tr>
    <? endforeach; ?>
    
  </tbody>
</table>

<?
$total = $data['tasks']['total'];
$page_num = $_GET['page'];
if( $page_num < 0 || empty($page_num) ) {
    $page_num = 1;
}
if( $page_num > $total ) {
    $page_num = $total;
}
?>
<div class="container">
    <nav>
      <ul class="pagination">
        <? if($page_num != 1): ?><li class="page-item"><a class="page-link" href="?page=<?=($page_num - 1)?>"><</a></li><? endif; ?>
        
        <? if($page_num - 2 > 0): ?><li class="page-item"><a class="page-link" href="?page=<?=($page_num - 2)?>"><?=($page_num - 2)?></a></li><? endif; ?>
        <? if($page_num - 1 > 0): ?><li class="page-item"><a class="page-link" href="?page=<?=($page_num - 1)?>"><?=($page_num - 1)?></a></li><? endif; ?>        
        <li class="page-item active"><a class="page-link" href="?page=<?=$page_num?>"><b><?=$page_num?></b></a></li>
        <? if($page_num + 1 <= $total): ?><li class="page-item"><a class="page-link" href="?page=<?=($page_num + 1)?>"><?=($page_num + 1)?></a></li><? endif; ?>
        <? if($page_num + 2 <= $total): ?><li class="page-item"><a class="page-link" href="?page=<?=($page_num + 2)?>"><?=($page_num + 2)?></a></li><? endif; ?>
                
        <? if($page_num != $total): ?><li class="page-item"><a class="page-link" href="?page=<?=($page_num + 1)?>">></a></li><? endif; ?>
      </ul>
    </nav>
</div>
