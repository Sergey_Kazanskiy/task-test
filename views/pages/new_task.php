<script>$('.alert').alert();</script>
<div class="container col-md-8">

      <h4 class="mb-3">Добавить новую задачу</h4>
        <?if( isset( $data['status'] ) ):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <strong>Успех!</strong> Данные успешно внесены! <a href="/">На главную</a>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <script>
        $(document).ready(function(){
            setTimeout( function(){
                $(".alert").fadeOut( 3500 );
            }, 2500);
        });
        </script>
        <?endif;?>

      <form class="needs-validation" action="/task/new" method="post">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="firstName">Имя или логин</label>
            <? if($data['is_admin']): ?>
            <input name="name" type="text" class="form-control" id="firstName" value="Admin" disabled="">
            <? else: ?>
            <input name="name" type="text" class="form-control" id="firstName" placeholder="Имя или логин" value="" required="">
            <? endif; ?>
          </div>
          <div class="col-md-6 mb-3">
          <label for="email">Email</label>
          <? if($data['is_admin']): ?>
          <input name="email" type="email" class="form-control" id="email" value="admin@test.local" disabled="">          
          <? else: ?>
          <input name="email" type="email" class="form-control" id="email" placeholder="you@example.com" required="">
          <? endif; ?>
          </div>
        </div>
        <textarea name="task" placeholder="Текст задачи..."  class="form-control" rows="5" required=""></textarea>
        <hr class="mb-4">
        <div class="container col-md-4">
            <button class="btn btn-primary btn-lg btn-block col-md-8 center" name="submit" type="submit">Отправить</button>
        </div>
        
      </form>

</div>