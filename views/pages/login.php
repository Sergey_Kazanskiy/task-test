<style>

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>

<form class="form-signin" action="login" method="POST">
    <? if( isset($data['err']) ): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <strong>Ошибка!</strong> неверные данные.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    <? endif; ?>

  <h1 class="h3 mb-3 font-weight-normal">Авторизация</h1>
  <label for="inputEmail" class="">Login</label>
  <input name="login" type="login" id="inputEmail" class="form-control" placeholder="Login" required autofocus>
  <label for="inputPassword" class="">Password</label>
  <input name="pass" type="password" id="inputPassword" class="form-control" placeholder="Password" required>

  <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
</form>