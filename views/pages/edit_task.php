<script>$('.alert').alert();</script>
<div class="container col-md-8">

      <h4 class="mb-3">Редактирование задачи (#<?=$_GET['id']?>)</h4>
        <?if(isset( $data['status'] ) && $data['status']):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <strong>Успех!</strong> Данные успешно внесены! <a href="/">На главную</a>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?endif;?>

      <form class="needs-validation" action="/task/edit/?id=<?=$_GET['id']?>" method="post">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="firstName">Имя или логин</label>
            <input name="name" type="text" class="form-control" id="firstName" placeholder="Имя или логин" value="<?=$data['login']?>" required="">
          </div>
          <div class="col-md-6 mb-3">
          <label for="email">Email</label>
          <input name="email" type="email" class="form-control" id="email" value="<?=$data['email']?>">          
          </div>
        </div>
        <textarea name="task" placeholder="Текст задачи..."  class="form-control" rows="5" required=""><?=$data['task']?></textarea>
        <hr class="mb-4">
        <div class="form-check">
          <? if( $data['complete'] ): ?>
          <input name="ready" class="form-check-input" type="checkbox" checked="" id="ready">
          <? else: ?>
          <input name="ready" class="form-check-input" type="checkbox" id="ready">
          <? endif; ?>
          <label class="form-check-label" for="defaultCheck1">
            Задача выполнена
          </label>
        </div>
        <hr class="mb-4">
        <div class="container col-md-4">
            <button class="btn btn-primary btn-lg btn-block col-md-8 center" name="submit" type="submit">Отправить</button>
        </div>
        
      </form>

</div>