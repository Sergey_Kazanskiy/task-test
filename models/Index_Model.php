<?
class Index_Model extends Models {
    public function getTasks( $count_pages=3, $page_num=1, $tbl_sort, $sort ) {
        $tasks_count = $this->query("SELECT * FROM `tasks`")->num_rows;
        $total = intval( ($tasks_count - 1) / $count_pages ) + 1;
        if( $page_num < 0 || empty($page_num) ) {
            $page_num = 1;
        }
        if( $page_num > $total ) {
            $page_num = $total;
        }
        $row_count = $page_num * $count_pages - $count_pages;        
        $page_num = $this->real_escape_string($page_num);
        
        $sort = $this->real_escape_string($sort);        
        if( $sort != 'desc' && $sort != 'asc' ){
            $sort = 'DESC';
        }        
        if( !empty( $tbl_sort ) ){
            $sql = "ORDER BY `$tbl_sort` $sort";
        }
             
        $q = $this->query("SELECT * FROM `tasks` $sql LIMIT $row_count, $count_pages");
        //echo "SELECT * FROM `tasks` $sql LIMIT $row_count, $count_pages";   
        while( $r = $q->fetch_assoc() ) {
            $rows[] = $r;
        }
        
        return array('rows'=>$rows,'total'=>$total);
    }
    //==============================================================================
    public function getTask( $id ){
        $id = $this->real_escape_string($id);
        return $this->query("SELECT * FROM `tasks` WHERE `id`='$id' ")->fetch_assoc();
    }
    //==============================================================================
    public function makeTask( $data_array, $admin ){
        $login = $this->real_escape_string( $data_array['name'] );
        $email = $this->real_escape_string( $data_array['email'] );
        $task = $this->real_escape_string( $data_array['task'] );
        if( $admin ){
            $login = 'Admin';
            $email = 'admin@test.local';
        }
        if( $this->query("INSERT INTO `tasks` SET `login`='$login',`email`='$email',`task`='$task'") ){
            return true;
        } else {
            return false;
        }
    }
    //==============================================================================
    public function editTask( $data_array ){
        $id = $this->real_escape_string( $data_array['id'] );
        $login = $this->real_escape_string( $data_array['name'] );
        $email = $this->real_escape_string( $data_array['email'] );
        $task = $this->real_escape_string( $data_array['task'] );
        $ready = $this->real_escape_string( $data_array['ready'] );
        $modern = $this->real_escape_string( $data_array['modern'] );        
        
        if( $ready == 'on' ){
            $ready = " ,`complete`='1' ";
        } else {
            $ready = " ,`complete`='0' ";
        }
        if( $modern ){
            $modern = " ,`modern`='1' ";
        }
        
        if( $this->query("UPDATE `tasks` SET `login`='$login',`email`='$email',`task`='$task' $ready $modern WHERE `id`='$id'") ){
            return true;
        } else {
            return false;
        }
    }
}
?>