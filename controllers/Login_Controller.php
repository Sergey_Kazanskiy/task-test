<?
class Login_Controller extends Controllers {
    public function login() {
        if( !$_SESSION['admin'] ){
            if( isset($_POST['login']) && isset($_POST['pass']) ){
                $pass = md5($_POST['pass']);
                $login = $_POST['login'];
                if( $pass === ADMIN_PASS && $login === ADMIN ){
                    $_SESSION['admin'] = 1;
                    header( "Location: http://".SITE );
                } else {
                    if( isset($_SESSION['admin']) ){
                        unset($_SESSION['admin']);
                    }
                    $data['err'] = 1;
                }
            }
            $this->tmpl("pages/login", $data );    
        } else {
            header( "Location: http://".SITE );
        }
    }
    //==============================================================================
    public function login_exit() {
        unset($_SESSION['admin']);
        if( !isset($_SESSION['admin']) ){
            header( "Location: http://".SITE );    
        }
    }
}
?>