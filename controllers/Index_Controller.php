<?php
class Index_Controller extends Controllers {
    public $model;
    
    public function __construct() {
        parent::__construct();
        $this->model = $this->getModel("Index");
    }
    public function init() {
        $data['is_admin'] = $this->admin;
        if( isset($_GET['login']) ){
            $tbl_sort = 'login';
            $sort = $_GET['login'];            
        } elseif( isset($_GET['email']) ){
            $tbl_sort = 'email';
            $sort = $_GET['email'];            
        } elseif( isset($_GET['status']) ){
            $tbl_sort = 'complete';
            $sort = $_GET['status'];            
        }
        
        $data['tasks'] = $this->model->getTasks( COUNT_PAGE, $_GET['page'], $tbl_sort, $sort );        
        $this->tmpl("pages/cont", $data );
    }
}