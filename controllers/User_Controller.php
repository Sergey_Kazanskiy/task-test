<?
class User_Controller extends Controllers {
    public $tmp;
    
    public function __construct() {
        parent::__construct();        
    }
    //==============================================================================
    public function task() {
        $page_id = $_GET['id'];
        $model = $this->getModel("Index");
        $data = $model->getTask($page_id);
        if( empty( $data ) ){
            $this->__404();
            return false;
        }
        $this->tmpl("pages/task_page", $data );
    }
    //==============================================================================
    public function task_new() {
        $model = $this->getModel("Index");
        if( !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['task']) && isset($_POST['submit']) ){            
            $data_array = array(
                    'name'=>$_POST['name'],
                    'email'=>$_POST['email'],
                    'task'=>$_POST['task']
            );
            $data['status'] = $model->makeTask( $data_array, $this->admin );
        } elseif( $this->admin && isset($_POST['submit']) ) {
            $data_array = array(
                    'task'=>$_POST['task']
            );
            $data['status'] = $model->makeTask( $data_array, $this->admin );            
        }
        $data['is_admin'] = $this->admin;
        $this->tmpl("pages/new_task", $data );
    }
    //==============================================================================
    public function task_edit() {
        $model = $this->getModel("Index");
        
        if( $this->admin ){
            $data = $model->getTask( $_GET['id'] );
            $data['status'] = 0;
            $this->tmp = $data['task'];

            if( isset($_POST['submit']) ){                
                if( $this->tmp != $_POST['task'] ){
                    $modern = 1;
                    $this->tmp = '';
                }
                $data_array = array(
                        'id'=>$_GET['id'],
                        'name'=>$_POST['name'],
                        'email'=>$_POST['email'],
                        'task'=>$_POST['task'],
                        'ready'=>$_POST['ready'],
                        'modern'=>$modern
                );                
                $status = $model->editTask( $data_array );
                $data = $model->getTask( $_GET['id'] );                
                $data['status'] = $status;
            }
            $data['is_admin'] = $this->admin;
            
            $this->tmpl("pages/edit_task", $data );    
        } else {
            $this->__404();
        }        
    }
}
?>